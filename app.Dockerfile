
FROM node:16

WORKDIR /usr/app

COPY package.json .

RUN yarn install

COPY ./dist/app .

CMD ["node", "/usr/app/server.js"]