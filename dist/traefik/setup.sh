#!/bin/bash
mkdir -p ../../data/traefik/certificates
mkdir -p ../../data/traefik/config/dynamic
cp dynamic/traefik.yml ../../data/traefik/config/dynamic/

openssl req -x509 -new -keyout ../../data/traefik/certificates/root.key -out ../../data/traefik/certificates/root.cer -config certificates/root.cnf

openssl req -nodes -new -keyout ../../data/traefik/certificates/server.key -out ../../data/traefik/certificates/server.csr -config certificates/server.cnf

openssl x509 -days 825 -req -in ../../data/traefik/certificates/server.csr -CA ../../data/traefik/certificates/root.cer -CAkey ../../data/traefik/certificates/root.key -set_serial 123 -out ../../data/traefik/certificates/server.cer -extfile certificates/server.cnf -extensions x509_ext
