const express = require('express');
const serveStatic = require('serve-static');
const logger = require('morgan');
const cors = require('cors');
const fs = require('fs');
const http = require('http');
const consola = require('consola');

// *** express instance *** //
const app = express();
app.use(logger('dev'));
app.use(cors());
app.use(serveStatic('.', { index: ['index.html'] }));

// catch 404 and forward to error handler
app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// *** error handlers *** //
app.use((err, req, res) => {
  res.json({ status: err.status || 500 });
  if (process.env.NODE_ENV === 'development') {
    res.render('error', {
      message: err.message,
      error: err,
    });
  }
});

/**
 * Get port from environment and store in Express.
 */
function normalizePort(val) {
  const port = parseInt(val, 10);
  // eslint-disable-next-line no-restricted-globals
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    // port number
    return port;
  }
  return false;
}
const port = normalizePort(process.env.APP_SERVER_PORT || '8080');
const host = process.env.APP_SERVER_HOST || '0.0.0.0';
app.set('port', port);

const server = http.createServer(app);

/**
 * Event listener for HTTP server "error" event.
 */
function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? `Pipe ${port}` : `Port ${port}`;

  // handle specific listen errors with friendly messages
  switch (error.code) {
    case 'EACCES':
      consola.error(`${bind} requires elevated privileges`);
      process.exit(1);
      break;
    case 'EADDRINUSE':
      consola.error(`${bind} is already in use`);
      process.exit(1);
      break;
    case 'EADDRNOTAVAIL:':
      consola.error(`${bind} is not available`);
      process.exit(1);
      break;
    default:
      throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */
function onListening() {
  const addr = server.address();
  const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
  consola.log(`Server listening on ${bind}`);
}

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, host);
server.on('error', onError);
server.on('listening', onListening);
